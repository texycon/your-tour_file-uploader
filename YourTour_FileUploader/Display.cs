﻿using System;

namespace YourTour_FileUploader
{
    public class Display
    {
        public static void Intro()
        {
            Console.WriteLine("Hello, loads of hardcoding here");
        }
        public static void Error(string error)
        {
            var before = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Error : " + error);

            Console.ForegroundColor = before;
        }

        internal static void SignIn(string server, string username, string password)
        {
            Console.WriteLine();
            Console.WriteLine("Contacting : " + server);
            Console.WriteLine();
            Console.WriteLine("username : " + username);
            Console.WriteLine("password : " + password);
            Console.WriteLine();
        }

        internal static void SignInResult(object token)
        {
            throw new NotImplementedException();
        }
        public static string GetAction()
        {
            while (true)
            {
                Console.WriteLine("input action (press h for help) and enter");
                var input = Console.ReadLine();
                switch (input.ToLowerInvariant())
                {
                    case "h":
                        PrintMenu();
                        break;
                    case "i":
                    case "u":
                    case "x":
                        return input;
                    default:
                        Console.WriteLine("input not recognised, try again");
                        Console.WriteLine();
                        break;
                }

            }
        }

        private static void PrintMenu()
        {
            Console.WriteLine("What to type: ");
            Console.WriteLine("* h - menu");
            Console.WriteLine("* x - exit");
            Console.WriteLine("* i - get image");
            Console.WriteLine("* u - upload file");
            Console.WriteLine("this is a really simple program what do you want from me?!");
        }

        public static void Outro()
        {
            Console.WriteLine("Done, press any key to exit");
            Console.ReadKey();
            Environment.Exit(0);
        }
    }
}

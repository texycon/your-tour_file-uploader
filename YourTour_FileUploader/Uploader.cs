﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace YourTour_FileUploader
{
    public class UploaderService
    {
        private readonly Uri _yourTourServerUrl;
        public UploaderService(Uri yourTourServerUrl)
        {
            _yourTourServerUrl = yourTourServerUrl;
        }

        /// <summary>
        /// Post form
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> GetTokenAsync(string username, string password)
        {
            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "password"     ),
                new KeyValuePair<string, string>("username", username ),
                new KeyValuePair<string, string>("password", password )
            });
            using (var http = GetClient())
            {
                var response = await http.PostAsync("api/auth/token", formContent);

                if (!response.IsSuccessStatusCode) { return response; }
                //TODO get token out of response

                return response;
            }
        }

        /// <summary>
        /// simple no auth Get
        /// </summary>
        /// <param name="imageId"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> GetImageAsync(Guid imageId)
        {
            using (var http = GetClient())
            {
                return await http.GetAsync("api/image/" + imageId.ToString());
            }
        }

        /// <summary>
        /// simple auth get
        /// </summary>
        /// <param name="imageId"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> GetMyToursAsync()
        {
            using (var http = GetClient())
            {
                return await http.GetAsync("/api/tour/built-by-me");
            }
        }


        private static StringContent GetPayload(object data)
        {
            var json = JsonConvert.SerializeObject(data);

            return new StringContent(json, Encoding.UTF8, "application/json");
        }

        private HttpClient GetClient()
        {
            var httpClient = new HttpClient
            {
                BaseAddress = _yourTourServerUrl
            };
            httpClient.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return httpClient;
        }

    }
}

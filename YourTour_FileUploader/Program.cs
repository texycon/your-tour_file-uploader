﻿using System;
using System.Threading.Tasks;

namespace YourTour_FileUploader
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Display.Intro();
            #region hard coded
            var prodServer = new YourTourServer()
            {
                Url = "https://yourtourservice.azurewebsites.net/",
                SampleImageId = Guid.Parse("97211358-0e09-4385-67ae-08d7509f7022"),
                MyUser = "",
                MyPass = "",
            };
            var testServer = new YourTourServer()
            {
                Url = "https://yourtourservice-test.azurewebsites.net/",
                SampleImageId = Guid.Parse("2575a90d-e939-44e2-f99d-08d5bcc0203c"),
                MyUser = "",
                MyPass = "",
            };
            #endregion
            var toUse = testServer;

           /////////////////////////
            Display.SignIn(toUse.Url, toUse.MyUser, toUse.MyPass );
            var uploader = new UploaderService(new Uri(toUse.Url));

            bool continueProgram = true;
            while (continueProgram)
            {
                try
                {
                    var action = Display.GetAction();
                    switch (action)
                    {
                        case "x":
                            continueProgram = false;
                            break;
                        case "i":
                            await ExecuteGetImage(uploader, toUse.SampleImageId);
                            break;
                        default:
                            await ExecuteUpload(uploader, toUse.MyUser, toUse.MyPass);
                            break;
                    }
                }
                catch (Exception e) { Display.Error(e.Message); }
            }
            Display.Outro();
        }

        private static async Task ExecuteGetImage(UploaderService uploader, Guid imageId)
        {

            var x = await uploader.GetImageAsync(imageId);
            Console.WriteLine("status code :" + x.StatusCode);

        }
        private static async Task ExecuteGetMyTours(UploaderService uploader)
        {

            var x = await uploader.GetMyToursAsync();
            Console.WriteLine("status code :" + x.StatusCode);

        }

        private static async Task ExecuteUpload(UploaderService uploader, string username, string password)
        {
            var token = await uploader.GetTokenAsync(username, password);
            Console.WriteLine("status code :" + token.StatusCode);
            // Display.SignInResult(token);
        }
    }
}
